﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logger
{
    /// <summary>
    /// Логгер
    /// </summary>
    public interface ILogger
    {

        /// <summary>
        /// Метод сохраняет переданное сообщение в лог.
        /// </summary>
        /// <param name="message">Сообщение, которое необходимо залогировать.</param>
        void Log(string message);
    }
}
