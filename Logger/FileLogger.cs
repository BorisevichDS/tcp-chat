﻿using System;
using System.Text;
using System.IO;

namespace Logger
{
    /// <summary>
    /// Класс содержит методы для логирования служебной информации в файл.
    /// </summary>
    public class FileLogger : ILogger
    {
        private static readonly string filePath = Directory.GetCurrentDirectory() + @"\log.txt";

        /// <summary>
        /// Метод сохраняет переданное сообщение в лог файл. 
        /// </summary>
        /// <param name="message">Сообщение, которое необходимо залогировать.</param>
        public void Log(string message)
        {
            using (FileStream fs = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.Write))
            {
                StreamWriter writer = new StreamWriter(fs, Encoding.UTF8);
                writer.WriteLine($"{DateTime.Now}: {message}");
                writer.Close();
            }
        }
    }
}
