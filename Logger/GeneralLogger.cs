﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logger
{
    public static class GeneralLogger
    {
        private static List<ILogger> _registeredLoggers = new List<ILogger>();

        /// <summary>
        /// Метод регистирирует экземпляр логгера.
        /// </summary>
        /// <param name="logger">Экземпляр логгера.</param>
        public static void RegisterHandler(ILogger logger)
        {
            _registeredLoggers.Add(logger);
        }

        /// <summary>
        /// Логирует сообщение.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        public static void Log(string message)
        {
            foreach (var el in _registeredLoggers)
            {
                el.Log(message);
            }
        }
    }
}
