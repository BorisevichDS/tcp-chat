﻿using System;

namespace Logger
{
    /// <summary>
    /// Вспомогательный класс для логирования в консоль.
    /// </summary>
    public class ConsoleLogger : ILogger
    {
        /// <summary>
        /// Метод выводит сообщение в консоль.
        /// </summary>
        /// <param name="message">Сообщение для логирования.</param>
        public void Log(string message)
        {
            ClearLine();
            Console.WriteLine($"{DateTime.Now}: {message}");
            Console.Write($"{DateTime.Now}: ");
        }

        /// <summary>
        /// Метод зачищает строку в консоли.
        /// </summary>
        private static void ClearLine()
        {
            int cursortop = Console.CursorTop;

            Console.SetCursorPosition(0, cursortop);
            Console.Write(new string(' ', Console.BufferWidth));
            Console.SetCursorPosition(0, cursortop);
        }
    }
}
