﻿using System;
using Newtonsoft.Json;

namespace Core
{
    /// <summary>
    /// Базовый класс сообщений.
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Идентификатор сообщения.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Дата и время создания.
        /// </summary>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Статус код сообщения.
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// Отправитель.
        /// </summary>
        public User Sender { get; set; }

        /// <summary>
        /// Текст сообщения.
        /// </summary>
        public string Text { get; set; }

        public MessageType MessageType { get; set; }

        public Message()
        {

        }
        public Message(string text, int statuscode)
        {
            Id = Guid.NewGuid();
            CreatedOn = DateTime.Now;
            Text = text;
            StatusCode = statuscode;
            MessageType = MessageType.ServiceMessage;
        }
        public Message(User sender, string text)
        {
            Id = Guid.NewGuid();
            CreatedOn = DateTime.Now;
            Text = text;
            Sender = sender;
            MessageType = MessageType.UserMessage;
        }

        public override string ToString()
        {
            string ret = string.Empty;
            switch (MessageType)
            {
                case MessageType.ServiceMessage:
                    ret = Text;
                    break;
                case MessageType.UserMessage:
                    ret = $"| {Sender}: {Text}";
                    break;
            }
            return ret;
        }
    }
    /// <summary>
    /// Тип сообщения.
    /// </summary>
    public enum MessageType
    {
        /// <summary>
        /// Пользовательское сообщение.
        /// </summary>
        UserMessage,
        /// <summary>
        /// Системное сообщение.
        /// </summary>
        ServiceMessage
    }
}
