﻿using System;
using Newtonsoft.Json;

namespace Core
{ 
    /// <summary>
    /// Пользователь.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        [JsonIgnore]
        public Guid Id { get; }

        /// <summary>
        /// Логин.
        /// </summary>
        public string Login { get; set; }

        public User()
        {
            Id = Guid.NewGuid();
        }
        public User(string login)
        {
            Id = Guid.NewGuid();
            Login = login;
        }

        public override string ToString()
        {
            return Login;
        }
    }
}
