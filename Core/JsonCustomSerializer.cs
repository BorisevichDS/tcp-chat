﻿using System;
using System.IO;
using Newtonsoft.Json;

namespace Core
{
    /// <summary>
    /// Отвечает за сериализацию/десериализацию объектов в JSON.
    /// </summary>
    public static class JsonCustomSerializer 
    {
        /// <summary>
        /// Сериализует объект в JSON.
        /// </summary>
        /// <param name="obj">Сериализуемый объект.</param>
        /// <returns></returns>
        public static string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented);
        }

        /// <summary>
        /// Десериализует JSON в объект.
        /// </summary>
        /// <typeparam name="T">Объект, в который десериализуется строка.</typeparam>
        /// <param name="json">JSON строка.</param>
        /// <returns></returns>
        public static T Deserialize<T>(string json) where T: class
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        private static void SerializeToFile(object obj)
        {
            string path = @"c:\Temp\chatDemo1.json";

            JsonSerializer serializer = new JsonSerializer();
            using (StreamWriter sw = File.CreateText(path))
            {
                serializer.Serialize(sw, obj);
            }
            Console.WriteLine("Сериализация прошла успешно.");
        }
    }
}
