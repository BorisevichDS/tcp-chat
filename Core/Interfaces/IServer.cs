﻿using System;

namespace Core
{
    /// <summary>
    /// Интерфейс сервера.
    /// </summary>
    public interface IServer : IDisposable
    {
        /// <summary>
        /// Метод отвечает за запуск сервера.
        /// </summary>
        void Start();

        /// <summary>
        /// Метод отвечает за остановку сервера.
        /// </summary>
        void Stop();

        /// <summary>
        /// Метод прослушивает сетевые подключения и при покдючении клиентов взаимодействует с ними.
        /// </summary>
        void ListenAsync();

    }
}
