﻿using System;


namespace Core
{
    /// <summary>
    /// Интерфейс, описываем клиент.
    /// </summary>
    public interface IClient : IDisposable
    {
        /// <summary>
        /// Метод отвечает за подключение клиента к серверу.
        /// </summary>
        /// <param name="ip">IP адрес сервера.</param>
        /// <param name="port">Порт.</param>
        void Connect(string ip, int port);

        /// <summary>
        /// Метод отвечает за отключение клиента от сервера.
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Метод отвечает за отправку сообщений на сервер.
        /// </summary>
        /// <param name="message">Передаваемое сообщение.</param>
        void SendMessage(Message message);

        /// <summary>
        /// Метод отвечает за получение сообщений с сервера.
        /// </summary>
        void RecieveMessage();
    }
}
