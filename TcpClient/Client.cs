﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Logger;
using Core;

namespace Client
{
    /// <summary>
    /// Класс, инкапуслирует логику работы клиента.
    /// </summary>
    internal class Client : IClient
    {
        private TcpClient _client;
        private NetworkStream _stream;
        private FileLogger _fileLogger;

        public Client()
        {
            _client = new TcpClient();
            _fileLogger = new FileLogger();
        }

        /// <summary>
        /// Метод отвечает за подключение клиента к серверу.
        /// </summary>
        /// <param name="ip">IP адрес сервера.</param>
        /// <param name="port">Порт.</param>
        public void Connect(string ip, int port)
        {
            IPAddress ipaddress = IPAddress.Parse(ip);
            _client.Connect(ipaddress, port);
            _stream = _client.GetStream();
            _fileLogger.Log($"Клиент подключен к серверу {ip}:{port}");
        }

        /// <summary>
        /// Метод отвечает за отключение клиента от сервера.
        /// </summary>
        public void Disconnect()
        {
            if (_stream != null)
            {
                _stream.Close();
            }

            if (_client != null)
            {
                _client.Close();
            }
            _fileLogger.Log($"Клиент отключен.");
        }

        /// <summary>
        /// Метод отвечает за отправку сообщений на сервер.
        /// </summary>
        /// <param name="message">Передаваемое сообщение.</param>
        public void SendMessage(Message message)
        {
            if (_stream != null)
            {
                string request = JsonCustomSerializer.Serialize(message);

                byte[] binarydata = Encoding.UTF8.GetBytes(request);
                _stream.Write(binarydata, 0, binarydata.Length);

                _fileLogger.Log("Запрос");
                _fileLogger.Log(request);
                _fileLogger.Log("Сообщение отправлено.");
            }
        }

        /// <summary>
        /// Метод отвечает за получение сообщений с сервера.
        /// </summary>
        public void RecieveMessage()
        {
            while (true)
            {
                StringBuilder response = new StringBuilder();
                if (_stream != null)
                {
                    byte[] binarydata = new byte[256];
                    do
                    {
                        _stream.Read(binarydata, 0, binarydata.Length);
                        response.Append(Encoding.UTF8.GetString(binarydata));
                    }
                    while (_stream.DataAvailable);
                }
                _fileLogger.Log("Ответ сервера.");

                // Дополнительно логируем ответ сервера в файл лога.
                _fileLogger.Log(response.ToString()); // JSON

                Message message = JsonCustomSerializer.Deserialize<Message>(response.ToString());

                _fileLogger.Log(message.ToString());

                if (message.MessageType == MessageType.UserMessage)
                {
                    GeneralLogger.Log(message.ToString());
                }
                Thread.Sleep(200);
            }
        }

        public void Dispose()
        {
            Disconnect();
        }
    }
}
