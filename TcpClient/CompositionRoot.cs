﻿using System;
using Logger;
using Core;

namespace Client
{
    internal static class CompositionRoot
    {
        private static IClient _client;
        
        public static IClient GetClient()
        {
            return _client;
        }

        public static void Install()
        {
            _client = new Client();

            ILogger consoleLogger = new ConsoleLogger();

            GeneralLogger.RegisterHandler(consoleLogger);
        }
    }
}
