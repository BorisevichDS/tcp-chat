﻿using System;
using System.Threading;
using Core;
using Logger;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            ILogger consoleLogger = new ConsoleLogger();
            try
            {
                CompositionRoot.Install();

                using (IClient client = CompositionRoot.GetClient())
                {
                    client.Connect("192.168.1.8", 8080);

                    Thread recieveThread = new Thread(new ThreadStart(client.RecieveMessage));
                    recieveThread.Start();

                    GeneralLogger.Log("Как Вас зовут?");

                    string username = Console.ReadLine().Trim();
                    User user = new User(username);

                    GeneralLogger.Log($"{username}, добро пожаловать в чат!");

                    while (true)
                    {
                        string inputmessage = Console.ReadLine();
                        Console.Write($"{DateTime.Now}: ");
                        string request = string.Empty;

                        if (inputmessage.Length > 0)
                        {
                            Message userMessage = new Message(user, inputmessage);
                            client.SendMessage(userMessage);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                GeneralLogger.Log(e.Message);
            }
            Console.ReadKey();
        }


    }
}
