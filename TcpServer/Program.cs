﻿using System;
using Logger;
using Core;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            CompositionRoot.Install();

            using (IServer server = CompositionRoot.GetServer())
            try
            {
                server.Start();
                server.ListenAsync();

            }
            catch (Exception e)
            {
                GeneralLogger.Log(e.Message);
                GeneralLogger.Log(e.StackTrace);
            }
            finally
            {
                Console.ReadKey();
            }
        }


    }
}
