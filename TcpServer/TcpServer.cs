﻿using System.Net.Sockets;
using System.Net;
using Core;
using Logger;
using System.Threading;

namespace Server
{
    /// <summary>
    /// Класс, описывает логику работы сервера.
    /// </summary>
    internal class TcpServer : IServer
    {
        private static TcpListener listener;

        public TcpServer(string ip, int port)
        {
            listener = new TcpListener(IPAddress.Parse(ip), port);
        }

        /// <summary>
        /// Метод отвечает за запуск сервера.
        /// </summary>
        public void Start()
        {
            listener.Start();
            GeneralLogger.Log("Сервер запущен.");
        }

        /// <summary>
        /// Метод отвечает за остановку сервера.
        /// </summary>
        public void Stop()
        {
            listener.Stop();
            GeneralLogger.Log("Сервер остановлен.");
        }

        /// <summary>
        /// Метод прослушивает сетевые подключения и при покдючении клиентов взаимодействует с ними.
        /// </summary>
        async public void ListenAsync()
        {
            GeneralLogger.Log("Слушаю...");
            
            while (true)
            {
                TcpClient newclient = await listener.AcceptTcpClientAsync();

                ClientObserver observer = new ClientObserver(newclient);

                Thread observerThread = new Thread(new ThreadStart(observer.GetMessage));
                observerThread.Start();
            }
        }

        public void Dispose()
        {
            Stop();
        }
    }
}
