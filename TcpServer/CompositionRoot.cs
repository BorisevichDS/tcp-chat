﻿using System;
using System.Collections.Generic;
using System.Text;
using Core;
using Logger;

namespace Server
{
    internal static class CompositionRoot
    {
        private static IServer _server;

        public static IServer GetServer()
        {
            return _server;
        }

        public static void Install()
        {
            _server = new TcpServer("192.168.1.8", 8080);

            ILogger consoleLogger = new ConsoleLogger();
            ILogger fileLogger = new FileLogger();

            GeneralLogger.RegisterHandler(consoleLogger);
            GeneralLogger.RegisterHandler(fileLogger);
        }

    }
}
