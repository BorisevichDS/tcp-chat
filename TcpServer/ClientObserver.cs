﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using Core;
using Logger;

namespace Server
{
    /// <summary>
    /// Наблюдатель за подключенными клиентами.
    /// </summary>
    class ClientObserver
    {
        private TcpClient _client;
        private static List<TcpClient> _clients = new List<TcpClient>();

        public ClientObserver(TcpClient client)
        {
            _client = client;
            RegisterClient(client);
        }

        /// <summary>
        /// Получает сообщения от клиента.
        /// </summary>
        public void GetMessage()
        {
            while (true)
            {
                Message serviceMessage = null;
                Message userMessage = null;
                try
                {
                    userMessage = RecieveMessage();
                    BroadcastMessage(userMessage);
                    serviceMessage = new Message("OK", 200);
                }
                catch (Exception)
                {
                    serviceMessage = new Message("Internal Server Error", 500);
                }
                SendMessage(_client, serviceMessage);
                Thread.Sleep(200);
            }
        }

        /// <summary>
        /// Метод получает сообщение от клиента.
        /// </summary>
        private Message RecieveMessage()
        {
            Message message = null;
            if (_client.Connected)
            {
                NetworkStream stream = _client.GetStream();
                byte[] binarydata = new byte[256];
                StringBuilder request = new StringBuilder();
                do
                {
                    stream.Read(binarydata, 0, binarydata.Length);
                    request.Append(Encoding.UTF8.GetString(binarydata));
                }
                while (stream.DataAvailable);

                GeneralLogger.Log("Получен запрос.");

                // Дополнительно логируем запрос от клиента в файл лога.
                GeneralLogger.Log("Запрос");
                GeneralLogger.Log(request.ToString());

                message = JsonCustomSerializer.Deserialize<Message>(request.ToString());

                // Дополнительно логируем ответ сервера в файл лога.
                GeneralLogger.Log("Ответ");
                GeneralLogger.Log(message.ToString());
            }
            return message;
        }

        /// <summary>
        /// Метод отправляет сообщение клиенту.
        /// </summary>
        /// <param name="response">Передаваемое сообщение.</param>
        private void SendMessage(TcpClient client, Message message)
        {
            if (client.Connected)
            {
                NetworkStream stream = client.GetStream();

                string response = JsonCustomSerializer.Serialize(message);
                
                byte[] binarydata = Encoding.UTF8.GetBytes(response);
                stream.Write(binarydata, 0, binarydata.Length);

                GeneralLogger.Log("Отправлен ответ.");

                // Дополнительно логируем запрос от сервера к клиенту в файл лога.
                GeneralLogger.Log("Запрос сервера к клиенту");
                GeneralLogger.Log(response);
            }
        }

        private void BroadcastMessage(Message userMessage)
        {
            if (userMessage == null)
            {
                return;
            }
            else
            {
                foreach (TcpClient c in _clients)
                {
                    if (c != _client)
                    {
                        SendMessage(c, userMessage);
                    }
                }
            }
        }

        /// <summary>
        /// Метод сохраненяет информацию о подключенных к серверу клиентах.
        /// </summary>
        /// <param name="client">Подключенный к серверу клиент.</param>
        public void RegisterClient(TcpClient client)
        {
            bool isClientRegistered = _clients.Exists(cl => cl.Equals(client));

            if (isClientRegistered == false)
            {
                _clients.Add(client);
                GeneralLogger.Log("Клиент зарегистрирован.");
            }
        }

        /// <summary>
        /// Метод удаляет информацию об отключенном клиенте.
        /// </summary>
        /// <param name="client"></param>
        public void RemoveClient(TcpClient client)
        {
            _clients.Remove(client);
            GeneralLogger.Log("Клиент отключен.");
        }
    }
}
